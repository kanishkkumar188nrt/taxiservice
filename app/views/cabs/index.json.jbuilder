json.array!(@cabs) do |cab|
  json.extract! cab, :id, :driver_name, :taxi_number
  json.url cab_url(cab, format: :json)
end
