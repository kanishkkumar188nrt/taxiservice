json.array!(@cab_locations) do |cab_location|
  json.extract! cab_location, :id, :longitude, :latitude, :cab_id
  json.url cab_location_url(cab_location, format: :json)
end
