class Cab < ActiveRecord::Base
	has_many :cab_locations
	with_options presence: true do |assoc|
		assoc.validates :driver_name
		assoc.validates :taxi_number
	end

	
end
