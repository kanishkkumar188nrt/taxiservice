class UserLocation < ActiveRecord::Base
  belongs_to :user, inverse_of: :user_locations
  with_options presence: true do |assoc|
		assoc.validates :longitude
		assoc.validates :latitude
		assoc.validates :user_id
	end
end
