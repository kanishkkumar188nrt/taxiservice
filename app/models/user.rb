class User < ActiveRecord::Base
	has_many :user_locations, inverse_of: :user
	with_options presence: true do |assoc|
		assoc.validates :name
		assoc.validates :email
	end
end
