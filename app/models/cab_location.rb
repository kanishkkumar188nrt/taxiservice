class CabLocation < ActiveRecord::Base
  	belongs_to :cab
  	with_options presence: true do |assoc|
		assoc.validates :longitude
		assoc.validates :latitude
		assoc.validates :cab_id
	end
	def self.get_nearest_cabs(user_location)
		self.find_by_sql("select * from cab_locations where SQRT(Power(? - latitude, 2) + Power(? - longitude, 2)) limit 10",user_location.latitude, user_location.longitude )
	end
end
