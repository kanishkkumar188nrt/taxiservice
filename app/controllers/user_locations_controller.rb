class UserLocationsController < ApplicationController
  before_action :fetch_user
  before_action :fetch_cabs, only: [:index]
  def new
    @user_location=UserLocation.new
  end

  def create
    @user_location=UserLocation.new(user_params)
    respond_to do |format|
      if @user_location.save
        format.html { redirect_to user_user_locations_path, notice: 'Cab location was successfully created.' }
        format.json { render :show, status: :created, location: @user_location }
      else
        format.html { render :new }
        format.json { render json: @user_location.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @userlocations=@user.user_locations
  end

  def update
  end

  def edit
  end

  def destroy
  end
  private
  def fetch_cabs
    @nearest_cabs=CabLocation.get_nearest_cabs(@user_location)
  end
  def fetch_user
    @user=User.find_by_id(params[:user_id])
  end
  def user_params
    params.require(:user_location).permit(:longitude, :latitude, :user_id)
  end
end
