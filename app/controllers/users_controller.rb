class UsersController < ApplicationController
  def new
    @user=User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'user was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    @users=User.all
  end

  def update
  end

  def edit
  end

  def destroy
  end
  def show
  end
  private
  def user_params
    params.require(:user).permit(:name, :email)
  end
end
