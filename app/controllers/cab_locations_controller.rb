class CabLocationsController < ApplicationController
  before_action :set_cab_location, only: [:show, :edit, :update, :destroy]

  # GET /cab_locations
  # GET /cab_locations.json
  def index
    @cab_locations = CabLocation.all
  end

  # GET /cab_locations/1
  # GET /cab_locations/1.json
  def show
  end

  # GET /cab_locations/new
  def new
    @cab_location = CabLocation.new
  end

  # GET /cab_locations/1/edit
  def edit
  end

  # POST /cab_locations
  # POST /cab_locations.json
  def create
    @cab_location = CabLocation.new(cab_location_params)

    respond_to do |format|
      if @cab_location.save
        format.html { redirect_to @cab_location, notice: 'Cab location was successfully created.' }
        format.json { render :show, status: :created, location: @cab_location }
      else
        format.html { render :new }
        format.json { render json: @cab_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cab_locations/1
  # PATCH/PUT /cab_locations/1.json
  def update
    respond_to do |format|
      if @cab_location.update(cab_location_params)
        format.html { redirect_to @cab_location, notice: 'Cab location was successfully updated.' }
        format.json { render :show, status: :ok, location: @cab_location }
      else
        format.html { render :edit }
        format.json { render json: @cab_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cab_locations/1
  # DELETE /cab_locations/1.json
  def destroy
    @cab_location.destroy
    respond_to do |format|
      format.html { redirect_to cab_locations_url, notice: 'Cab location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cab_location
      @cab_location = CabLocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cab_location_params
      params.require(:cab_location).permit(:longitude, :latitude, :cab_id)
    end
end
