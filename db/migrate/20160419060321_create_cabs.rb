class CreateCabs < ActiveRecord::Migration
  def change
    create_table :cabs do |t|
      t.string :driver_name
      t.string :taxi_number

      t.timestamps null: false
    end
  end
end
