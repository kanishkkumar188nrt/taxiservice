class CreateUserLocations < ActiveRecord::Migration
  def change
    create_table :user_locations do |t|
      t.string :longitude
      t.string :latitude
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
