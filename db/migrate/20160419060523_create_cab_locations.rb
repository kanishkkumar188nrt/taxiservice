class CreateCabLocations < ActiveRecord::Migration
  def change
    create_table :cab_locations do |t|
      t.string :longitude
      t.string :latitude
      t.references :cab, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
