require 'test_helper'

class CabLocationsControllerTest < ActionController::TestCase
  setup do
    @cab_location = cab_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cab_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cab_location" do
    assert_difference('CabLocation.count') do
      post :create, cab_location: { cab_id: @cab_location.cab_id, latitude: @cab_location.latitude, longitude: @cab_location.longitude }
    end

    assert_redirected_to cab_location_path(assigns(:cab_location))
  end

  test "should show cab_location" do
    get :show, id: @cab_location
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cab_location
    assert_response :success
  end

  test "should update cab_location" do
    patch :update, id: @cab_location, cab_location: { cab_id: @cab_location.cab_id, latitude: @cab_location.latitude, longitude: @cab_location.longitude }
    assert_redirected_to cab_location_path(assigns(:cab_location))
  end

  test "should destroy cab_location" do
    assert_difference('CabLocation.count', -1) do
      delete :destroy, id: @cab_location
    end

    assert_redirected_to cab_locations_path
  end
end
